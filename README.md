# retry [![build status](https://gitlab.com/TheZxcv/retry/badges/master/build.svg)](https://gitlab.com/TheZxcv/retry/commits/master)
Command line utility to retry a command until it succeeds.

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```
## Usage

```bash
$ retry [COMMAND]
```
