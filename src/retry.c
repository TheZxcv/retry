#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

static const char *program_name = "retry";
static unsigned long long delay_millis = 500;

/* Exit statuses for programs like 'env' that exec other programs.
   Copied from coreutils' system.h */
enum {
	EXIT_TIMEDOUT = 124,      /* Time expired before child completed.  */
	EXIT_CANCELED = 125,      /* Internal error prior to exec attempt.  */
	EXIT_CANNOT_INVOKE = 126, /* Program located, but not usable.  */
	EXIT_ENOENT = 127         /* Could not find program to exec.  */
};

typedef void (*sighandler_t)(int);

void delay(unsigned long long millis) {
	usleep(millis * 1000U);
}

noreturn void usage(int exit_code) {
	printf("Usage: %s [-d DELAY] [COMMAND]\n", program_name);
	puts("Retry the given command until it succeeds.\n");
	puts(" -d DELAY    optional delay between retries in milliseconds (default: 500ms)");
	puts(" -h          display this help and exit");
	exit(exit_code);
}

static char **parse_args(int argc, char **argv) {
	opterr = 0;
	errno = 0;

	int c;
	while ((c = getopt(argc, argv, "d:h")) != -1) {
		switch (c) {
			case 'd': {
				char *endptr;
				delay_millis = strtoull(optarg, &endptr, 10);
				if (errno != 0) {
					fprintf(stderr, "error while parsing delay: %s\n", strerror(errno));
					exit(EXIT_FAILURE);
				} else if ((size_t)(endptr - optarg) != strlen(optarg)) {
					fprintf(stderr, "error: '%s' is not a valid delay\n", optarg);
					exit(EXIT_FAILURE);
				}
			} break;
			case 'h': usage(EXIT_SUCCESS); break;
			case '?': {
				if (optopt == 'd')
					fprintf(stderr, "option -%c requires an argument.\n", optopt);
				else
					fprintf(stderr, "unknown option `-%c'.\n", optopt);
				usage(EXIT_FAILURE);
			} break;
			default: abort();
		}
	}

	return &argv[optind];
}

static int run_command(char *const *cmd) {
	int status;
	int exec_errno;

	/* Unnamed pipe to check for successful exec. */
	int error_pipe[2]; /* [0] read end, [1] write end */
	if (pipe(error_pipe) == -1) {
		fprintf(stderr, "cannot create unnamed pipe\n");
		exit(EXIT_FAILURE);
	}

	/*
	 * 1. Open a pipe in the parent process.
	 * 2. After forking, the parent closes the writing end of the pipe and
	 * reads from the reading end.
	 * 3. The child closes the reading end and sets the close-on-exec flag for
	 * the writing end.
	 * 4. The child calls exec.
	 * 5. If exec fails, the child writes the error code back to the parent
	 * using the pipe, then exits.
	 * 6. The parent reads eof (a zero-length read) if the child successfully
	 * performed exec, since close-on-exec made successful exec close the
	 * writing end of the pipe. Or, if exec failed, the parent reads the error
	 * code and can proceed accordingly. Either way, the parent blocks until
	 * the child calls exec.
	 * 7. The parent closes the reading end of the pipe.
	 *
	 * Reference: https://stackoverflow.com/a/3703179
	 */
	pid_t pid = fork();
	if (pid < 0) {
		fprintf(stderr, "cannot fork\n");
		exit(EXIT_FAILURE);
	} else if (pid == 0) {
		/* child */
		close(error_pipe[0]);
		/* Setting close-on-exec flag sends EOF to the parent if exec is
		 * performed successfully */
		if (fcntl(error_pipe[1], F_SETFD, FD_CLOEXEC) == -1) {
			fprintf(stderr, "failed to set close-on-exec flag for the error pipe\n");
			exit(EXIT_FAILURE);
		}

		execvp(cmd[0], cmd);
		exec_errno = errno;
		fprintf(stderr, "cannot run %s\n", cmd[0]);

		/* Send the errno from exec to the parent on failure */
		write(error_pipe[1], &exec_errno, sizeof(exec_errno));
		close(error_pipe[1]);

		exit(exec_errno == ENOENT ? EXIT_ENOENT : EXIT_CANNOT_INVOKE);
	} else {
		/* parent */
		close(error_pipe[1]);
		/* Reads errno if the child failed, otherwise EOF */
		int data_read = read(error_pipe[0], &exec_errno, sizeof(exec_errno));
		if (data_read != 0)
			exit(exec_errno == ENOENT ? EXIT_ENOENT : EXIT_CANNOT_INVOKE);
		close(error_pipe[0]);
	}

	/* Disable signals for self but not for the child */
	sighandler_t interrupt_signal = signal(SIGINT, SIG_IGN);
	sighandler_t quit_signal = signal(SIGQUIT, SIG_IGN);

	pid = wait(&status);
	if (pid == -1) {
		fprintf(stderr, "error waiting for child process\n");
		exit(EXIT_FAILURE);
	}

	/* Re-enable signals.  */
	signal(SIGINT, interrupt_signal);
	signal(SIGQUIT, quit_signal);

	if (WIFEXITED(status)) {
		return WEXITSTATUS(status);
	} else if (WIFSIGNALED(status)) {
		return EXIT_SUCCESS; // child was killed by a signal.
	} else {
		return status;
	}
}

int retry_main(int argc, char **argv) {
	if (argc == 1) {
		fprintf(stderr, "missing program to run\n");
		usage(EXIT_FAILURE);
	}

	int status;

	program_name = argv[0];
	char **command_line = parse_args(argc, argv);
	do {
		status = run_command(command_line);
		if (status == EXIT_SUCCESS)
			return EXIT_SUCCESS;
		if (delay_millis > 0)
			delay(delay_millis);
	} while (true);
}
