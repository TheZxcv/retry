#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "retry.h"

static void test_none(void **state) {
	(void) state; /* unused */
	assert_int_equal(0, 0);
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_none),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
